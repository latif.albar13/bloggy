Bloggy Blog

Feature
1. Add new Posting
2. Edit Posting
3. Delete Posting

Device testing
Infinix Hot11 with Android 11 OS.

[-] App

    [-] Adapter
        // Collection of binding, included binding adapter and recyclerview adapter

    [-] Base
        // Contain Application and base for activity and fragment

    [-] Main
        // Starter Activity are here (MainActivity)

        [-] ViewModel
            // MainActivity's view model

    [-] Model
        // Collection of all model response from server.

    [-] Network
        [-] Module
            // Collection of all modules for Dependency injection purpose.

        [-] Repository
            // Repository collection

        [-] Service
            // All service api listed here



-----------------  Github for this project  -----------------
https://gitlab.com/latif.albar13/bloggy
branch : [MASTER]