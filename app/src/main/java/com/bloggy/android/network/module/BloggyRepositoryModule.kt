package com.bloggy.android.network.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.bloggy.android.network.repository.BloggyRepository
import com.bloggy.android.network.service.BlogServices
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class BloggyRepositoryModule {

    @Provides
    @Singleton
    fun provideBloggyRepository(blogServices: BlogServices): BloggyRepository {
        return BloggyRepository(blogServices)
    }
}