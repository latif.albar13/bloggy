package com.bloggy.android.network.repository

import com.bloggy.android.model.BlogData
import com.bloggy.android.model.BlogRequest
import com.bloggy.android.network.service.BlogServices
import io.reactivex.Observable

class BloggyRepository(private var blogServices: BlogServices) {
    fun getBlogList(): Observable<List<BlogData>> {
        return blogServices.getBlog()
            .onErrorResumeNext { it: Throwable ->
                Observable.just(mutableListOf())
            }
    }

    fun getBlogDetail(id: Int): Observable<BlogData> {
        return blogServices.getBlogDetail(id)
            .onErrorResumeNext { it: Throwable ->
                Observable.just(BlogData())
            }
    }

    fun deletePost(id: Int): Observable<BlogData> {
        return blogServices.deletePost(id)
    }

    fun update(id: Int, body: BlogRequest): Observable<BlogData> {
        return blogServices.updatePost(id, body)
    }

    fun create(body: BlogRequest): Observable<BlogData> {
        return blogServices.createPost(body)
    }


}