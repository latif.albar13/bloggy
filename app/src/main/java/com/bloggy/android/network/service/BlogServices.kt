package com.bloggy.android.network.service

import com.bloggy.android.model.BlogData
import com.bloggy.android.model.BlogRequest
import io.reactivex.Observable
import retrofit2.http.*

interface BlogServices {

    @GET("posts")
    fun getBlog(): Observable<List<BlogData>>

    @GET("posts/{id}")
    fun getBlogDetail(@Path("id") id: Int): Observable<BlogData>

    @DELETE("posts/{id}")
    fun deletePost(
        @Path("id") id: Int): Observable<BlogData>

    @PUT("posts/{id}")
    fun updatePost(@Path("id") id: Int, @Body payload: BlogRequest): Observable<BlogData>

    @POST("posts")
    fun createPost(@Body payload: BlogRequest): Observable<BlogData>



}