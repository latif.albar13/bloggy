package com.bloggy.android.main

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bloggy.android.R
import com.bloggy.android.adapter.BlogAdapter
import com.bloggy.android.base.BaseActivity
import com.bloggy.android.databinding.ActivityMainBinding
import com.bloggy.android.main.viewmodel.MainViewModel
import com.bloggy.android.model.BlogData
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val vm: MainViewModel by viewModels()
    private var mBottomSheetBehavior: BottomSheetBehavior<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initRecyclerLinearLayout()
        initItemClickListener()
        initItemLongClickListener()
        initBinding()
        initActionListener()
    }

    override fun onResume() {
        super.onResume()
        vm.getBlogList()
    }

    private fun initBinding() {
        with(binding) {
            lifecycleOwner = this@MainActivity
            viewModel = vm
        }
    }

    private fun initActionListener() {
        with(vm) {
            isBlogFinishLoaded.observe(this@MainActivity, Observer {
                if (it) {
                    getBlogAdapter().addAll(vm.mListBlog)
                }
            })
            isLoading.observe(this@MainActivity, Observer {
                if (it) {
                    mBottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
                }
            })
            openNewPost.observe(this@MainActivity, Observer {
                if (it) {
                    val intent = Intent(this@MainActivity, EditActivity::class.java)
                    intent.putExtra("new_post", true)
                    startActivity(intent)
                }
            })
        }
    }

    private fun initRecyclerLinearLayout() {
        binding.rvBlog.layoutManager = LinearLayoutManager(this)
    }

    private fun initItemClickListener() {
        getBlogAdapter().setItemBlogCallback(object : BlogAdapter.ItemBlogClickListener {
            override fun onBlogClick(position: Int, blog: BlogData) {
                val intent = Intent(this@MainActivity, DetailActivity::class.java)
                intent.putExtra("id", blog.id)
                startActivity(intent)
            }
        })
    }

    private fun initItemLongClickListener() {
        getBlogAdapter().setLongClickListener(object : BlogAdapter.ItemBlogLongClickListener {
            override fun onBlogClick(position: Int, blog: BlogData) {

            }
        })
    }

    private fun getBlogAdapter(): BlogAdapter {
        val adapter: BlogAdapter
        if (binding.rvBlog.adapter != null) {
            adapter = binding.rvBlog.adapter as BlogAdapter
        } else {
            adapter = BlogAdapter()
            binding.rvBlog.adapter = adapter
        }
        return adapter
    }
}

