package com.bloggy.android.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bloggy.android.R
import com.bloggy.android.base.BaseActivity
import com.bloggy.android.databinding.ActivityDetailBinding
import com.bloggy.android.main.viewmodel.DetailViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailActivity : BaseActivity() {
    private lateinit var binding: ActivityDetailBinding
    private val vm: DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)

        initBinding()
        initDetail()
        initActionListener()

    }

    private fun initBinding() {
        with(binding) {
            lifecycleOwner = this@DetailActivity
            viewModel = vm
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initActionListener() {
        with(vm) {
            mBlogData.observe(this@DetailActivity, Observer {
                if (it.id != 0) {
                    binding.mBlog = it
                    binding.wvContent.settings.javaScriptEnabled = true
                    binding.wvContent.loadData(it.content, "text/html; charset=utf-8", "UTF-8")
                }
            })
            doDelete.observe(this@DetailActivity, Observer {
                if (it.id != 0) {
                    vm.deletePost(it.id)
                }
            })
            doEdit.observe(this@DetailActivity, Observer {
                if (it.id != 0) {
                    val intent = Intent(this@DetailActivity, EditActivity::class.java)
                    intent.putExtra("blog_data", Gson().toJson(it))
                    startActivity(intent)
                }
            })

            isDeleteFinish.observe(this@DetailActivity, Observer {
                if (it) {
                    onBackPressed()
                }
            })

        }
    }


    private fun initDetail() {
        val id = intent.getIntExtra("id", 0)
        vm.getBlogDetail(id)
    }
}