package com.bloggy.android.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bloggy.android.model.BlogData
import com.bloggy.android.model.BlogRequest
import com.bloggy.android.network.repository.BloggyRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class EditViewModel @Inject constructor(var repository: BloggyRepository) : ViewModel() {
    var isLoading = MutableLiveData<Boolean>().apply { value = false }
    var title = MutableLiveData<String>()
    var content = MutableLiveData<String>()
    var isFinishSaving = MutableLiveData<Boolean>().apply { value = false }

    fun update(blogData: BlogData) {
        isLoading.postValue(true)
        val data = BlogRequest(blogData.title, blogData.content)
        repository.update(blogData.id, data)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                isLoading.postValue(false)
                isFinishSaving.postValue(true)
            }
            .subscribe()
    }

    fun create(blogData: BlogData) {
        isLoading.postValue(true)
        val data = BlogRequest(blogData.title, blogData.content)
        repository.create(data)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                isLoading.postValue(false)
                isFinishSaving.postValue(true)
            }
            .subscribe()
    }
}