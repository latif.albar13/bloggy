package com.bloggy.android.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bloggy.android.model.BlogData
import dagger.hilt.android.lifecycle.HiltViewModel
import com.bloggy.android.network.repository.BloggyRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(var repository: BloggyRepository) : ViewModel() {

    val mListBlog = mutableListOf<BlogData>()
    val isBlogFinishLoaded = MutableLiveData<Boolean>().apply { value = false }
    var isLoading = MutableLiveData<Boolean>().apply { value = false }
    var openNewPost = MutableLiveData<Boolean>().apply { value = false }

    fun getBlogList() {
        isLoading.postValue(true)
        repository.getBlogList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                isLoading.postValue(false)
                mListBlog.clear()
                mListBlog.addAll(it)
                isBlogFinishLoaded.postValue(true)
            }
            .subscribe()
    }

    fun newPost() {
        openNewPost.postValue(true)
    }
}