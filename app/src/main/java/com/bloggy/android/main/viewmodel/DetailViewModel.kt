package com.bloggy.android.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bloggy.android.model.BlogData
import com.bloggy.android.model.BlogRequest
import com.bloggy.android.network.repository.BloggyRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(var repository: BloggyRepository) : ViewModel() {
    var isLoading = MutableLiveData<Boolean>().apply { value = false }
    val isFinishLoad = MutableLiveData<Boolean>().apply { value = false }
    var mBlogData  = MutableLiveData<BlogData>().apply { value = BlogData() }
    var doDelete = MutableLiveData<BlogData>().apply { value = BlogData() }
    var doEdit = MutableLiveData<BlogData>().apply { value = BlogData() }
    val isDeleteFinish = MutableLiveData<Boolean>().apply { value = false }


    fun getBlogDetail(id: Int) {
        isLoading.postValue(true)
        repository.getBlogDetail(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {

                isLoading.postValue(false)
                mBlogData.postValue(it)
                isFinishLoad.postValue(true)
            }
            .subscribe()
    }

    fun getEditInfo(blog: BlogData) {
        doEdit.postValue(blog)
    }

    fun getDeleteInfo(blog: BlogData) {
        doDelete.postValue(blog)
    }

    fun deletePost(id: Int) {
        isLoading.postValue(true)
        repository.deletePost(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                isLoading.postValue(false)
                isDeleteFinish.postValue(true)
            }
            .subscribe()
    }
}