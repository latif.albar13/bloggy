package com.bloggy.android.main

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bloggy.android.R
import com.bloggy.android.base.BaseActivity
import com.bloggy.android.databinding.ActivityEditBinding
import com.bloggy.android.main.viewmodel.EditViewModel
import com.bloggy.android.model.BlogData
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_edit.*

@AndroidEntryPoint
class EditActivity : BaseActivity() {
    private lateinit var binding: ActivityEditBinding
    private val vm: EditViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit)

        initBinding()
        initActionListener()
        initInputData()

    }

    private fun initBinding() {
        with(binding) {
            lifecycleOwner = this@EditActivity
            viewModel = vm
        }
    }

    private fun initActionListener() {
        with(vm) {
            isLoading.observe(this@EditActivity, Observer {
                if (it) {
                    val intent = Intent(this@EditActivity, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            })
            isFinishSaving.observe(this@EditActivity, Observer {
                if (it) {
                    Toast.makeText(this@EditActivity, "update success", Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    private fun initInputData() {
        val isNewPost = intent.getBooleanExtra("new_post", false)
        if (!isNewPost) {
            val json = intent.getStringExtra("blog_data")
            val blogData = Gson().fromJson(json, BlogData::class.java)
            binding.mBlog = blogData
            etTitle.setText(blogData.title)
            etContent.setText(blogData.content)
            binding.action = "edit"
        } else {
            binding.mBlog = BlogData()
            binding.action = "new"
        }
    }
}