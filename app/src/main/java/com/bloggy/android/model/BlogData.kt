package com.bloggy.android.model

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat

data class BlogData(
    var id: Int = 0,
    var title: String = "",
    var content: String = "",
    @SerializedName("published_at") var publishedAt: String = "",
    @SerializedName("updated_at") var updatedAt: String = ""
) {
    fun getDate(): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val date = dateFormat.parse(publishedAt)
        val newFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val newDate = newFormat.format(date)
        return "dipublish pada : $newDate"
    }
}