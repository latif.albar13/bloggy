package com.bloggy.android.model

data class BlogRequest(var title: String = "", var content: String = "")