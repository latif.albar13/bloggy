package com.bloggy.android.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bloggy.android.databinding.ItemBlogBinding
import com.bloggy.android.model.BlogData

class BlogAdapter : RecyclerView.Adapter<BlogAdapter.ViewHolder>() {

    private var mList = mutableListOf<BlogData>()
    private lateinit var mClickListener: ItemBlogClickListener
    private lateinit var mLongClickListener: ItemBlogLongClickListener

    fun addAll(list: MutableList<BlogData>) {
        mList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemBlogBinding.inflate(inflater))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(mList[position], position)

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ViewHolder(val itemBinding: ItemBlogBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(blog: BlogData, position: Int) {
            itemBinding.blog = blog
            itemBinding.executePendingBindings()
            itemBinding.root.setOnClickListener {
                mClickListener.onBlogClick(position, blog)
            }
        }
    }

    interface ItemBlogClickListener {
        fun onBlogClick(position: Int, blog: BlogData)
    }

    fun setItemBlogCallback(listener: ItemBlogClickListener) {
        this.mClickListener = listener
    }

    interface ItemBlogLongClickListener {
        fun onBlogClick(position: Int, blog: BlogData)
    }

    fun setLongClickListener(listener: ItemBlogLongClickListener) {
        this.mLongClickListener = listener
    }

}